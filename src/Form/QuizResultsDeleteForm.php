<?php

namespace Drupal\quizard\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Quiz results entities.
 *
 * @ingroup quizard
 */
class QuizResultsDeleteForm extends ContentEntityDeleteForm {

}
