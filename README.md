Quizard
=======

The Quizard module provides Administrators an interface for creating quiz templates and users with a quiz wizard.

Installation
------------
To install, place the quizard module in your modules directory. Navigate Manage > Extend, and enable Quizard.

Usage
-----
1. Add a quiz by navigating Manage > Content > Add content > Quiz.
2. After saving a quiz you should arrive at /node/# where # is the node id. Navigate to /quiz/# to take the quiz.
3. Administrators can navigate Manage > Content > Quizard to see a list of quiz results.

Configure
---------
You can configure Quizard by navigating Manage > Configuration > Workflow > Quizard Settings.

